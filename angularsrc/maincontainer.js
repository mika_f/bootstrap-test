﻿var app = angular.module('myApp', []);
app.controller('maincontainer', function ($scope) {
    $scope.listgroup = [
        { name: 'Alian', img: 'alien.png', title: 'Hello, earth', text: 'I am Alien' },
        { name: 'Mask', img: 'mask.png', title: 'Hello', text: 'V for Vendetta' }
    ];

    $scope.onAdd = function () {
        this.listgorup.push({
            name: this.name,
            img: this.img,
            title: this.title,
            text: this.text,
        });
        this.name = this.img = this.title = this.text = "";
    };

});
